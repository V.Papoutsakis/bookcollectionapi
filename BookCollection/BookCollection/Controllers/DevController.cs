﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookCollection.Data;
using BookCollection.Models.ApiResponses.Base;
using BookCollection.Models.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BookCollection.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;

        public DevController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        [HttpGet("up_migrate")]
        public APIResult<string> Migrate() 
        {
            try
            {
                _appDbContext.Database.Migrate();
            }
            catch (Exception e)
            {

                return new APIResult<string>(e);
            }
            return new APIResult<string>("OK");
        }
    }
}
