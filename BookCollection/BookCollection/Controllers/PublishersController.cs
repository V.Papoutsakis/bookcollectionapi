﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookCollection.Models.ApiRequests;
using BookCollection.Models.ApiResponses.Base;
using BookCollection.Models.DTO;
using BookCollection.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BookCollection.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublishersController : ControllerBase
    {
        private readonly PublishersService _publishersService;
        private readonly ILogger<PublishersController> _logger;

        public PublishersController(PublishersService publishersService, ILogger<PublishersController> logger)
        {
            _publishersService = publishersService;
            _logger = logger;
        }

        /// <summary>
        /// Create new Publisher | also add reference to multiple books by Id's or by ISBN 
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// APIResult CreateEntityAndBindingsResult Guid
        /// </returns>
        [HttpPost]
        public async Task<APIResult<CreateEntityAndBindingsResult<Guid>>> Create([FromBody] PublisherCreate publisherCreateRequest)
        {
            var result = await _publishersService.CreateAndBind(publisherCreateRequest);
            if (result.Status)
            {
                return new APIResult<CreateEntityAndBindingsResult<Guid>>(result.Data);
            }
            return new APIResult<CreateEntityAndBindingsResult<Guid>>(Constants.ProjectErrorCodes.InvalidPayload);
        }
    }
}
