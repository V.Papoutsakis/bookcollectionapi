﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookCollection.Models.ApiRequests;
using BookCollection.Models.ApiResponses.Base;
using BookCollection.Models.DTO;
using BookCollection.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BookCollection.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly AuthorsService _authorsService;
        private readonly ILogger<AuthorsController> _logger;

        public AuthorsController(AuthorsService authorsService, ILogger<AuthorsController> logger)
        {
            _authorsService = authorsService;
            _logger = logger;
        }

        /// <summary>
        /// Create new Author | also add reference to multiple books by Id's or by ISBN 
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// APIResult CreateEntityAndBindingsResult Guid
        /// </returns>
        [HttpPost]
        public async Task<APIResult<CreateEntityAndBindingsResult<Guid>>> Create([FromBody] AuthorCreate authorCreateRequest)
        {
            var result = await _authorsService.CreateAndBind(authorCreateRequest);
            if (result.Status)
            {
                return new APIResult<CreateEntityAndBindingsResult<Guid>>(result.Data);
            }
            return new APIResult<CreateEntityAndBindingsResult<Guid>>(Constants.ProjectErrorCodes.InvalidPayload);
        }
    }
}
