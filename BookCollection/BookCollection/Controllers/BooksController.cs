﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookCollection.Constants;
using BookCollection.Data;
using BookCollection.Models.ApiRequests;
using BookCollection.Models.ApiResponses;
using BookCollection.Models.ApiResponses.Base;
using BookCollection.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BookCollection.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BooksService _booksService;
        private readonly ILogger<BooksController> _logger;

        public BooksController(BooksService booksService, ILogger<BooksController> logger) 
        {
            _booksService = booksService;
            _logger = logger;
        }

        /// <summary>
        /// Get List Of Books | Order  Choice => ALPHA = 1, ALPHACUSTOMORDER = 2
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// APIResult Guid
        /// </returns>
        [HttpGet("{orderChoice}")]
        public async Task<APIResult<List<BookListItemResult>>> GetList(OrderChoice orderChoice)
        {
            var results = await _booksService.GetBookList(orderChoice);

            if (results.Status)
            {
                return new APIResult<List<BookListItemResult>>(results.Data);
            }
            return new APIResult<List<BookListItemResult>>(ProjectErrorCodes.GenericError);
        }


        [HttpGet("id/{bookId}")]
        public async Task<APIResult<BookItemDetailsResult>> GetBook(Guid bookId)
        {
            var results = await _booksService.GetBookDetails(bookId);

            if (results.Status)
            {
                return new APIResult<BookItemDetailsResult>(results.Data);
            }
            return new APIResult<BookItemDetailsResult>(ProjectErrorCodes.NotExisting);
        }

        /// <summary>
        /// Create new Book | also add reference to author and publisher 
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// APIResult Guid
        /// </returns>
        [HttpPost]
        public async Task<APIResult<Guid?>> Create([FromBody] BookCreate bookCreateRequest) 
        {
            var result = await _booksService.Create(bookCreateRequest);
            if (result.Status)
            {
                return new APIResult<Guid?>(result.Data);
            }
            return new APIResult<Guid?>(Constants.ProjectErrorCodes.InvalidPayload);
        }

        /// <summary>
        /// Update Book | also change reference to author and publisher 
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>
        /// APIResult Guid
        /// </returns>
        [HttpPut]
        public async Task<APIResult<Guid?>> Update([FromBody] BookUpdate bookUpdateRequest)
        {
            var result = await _booksService.Update(bookUpdateRequest);
            if (result.Status)
            {
                return new APIResult<Guid?>(result.Data);
            }
            return new APIResult<Guid?>(Constants.ProjectErrorCodes.InvalidPayload);
        }

        [HttpDelete("{bookId}")]
        public async Task<APIResult<Guid?>> Delete(Guid bookId)
        {
            var result = await _booksService.Delete(bookId);
            if (result.Status)
            {
                return new APIResult<Guid?>(result.Data);
            }
            return new APIResult<Guid?>(Constants.ProjectErrorCodes.InvalidPayload);
        }
    }
}