﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Helpers
{
    public static class ISBNValidator
    {
        public static string NormalizeIsbn(this string isbn)
        {
            return isbn.Replace("-", "").Replace(" ", "");
        }
        public static bool CheckIsbn(this string isbn) 
        {
            if (isbn == null)
                return false;

            isbn = isbn.NormalizeIsbn();
            return IsValidIsbn10(isbn) || IsValidIsbn13(isbn);
        }

        public static bool IsValidIsbn10(this string isbn10)
        {
            if (string.IsNullOrEmpty(isbn10))
            {
                return false;
            }

            if (isbn10.Contains("-"))
            {
                isbn10 = isbn10.Replace("-", string.Empty);
            }

            if (isbn10.Length != 10)
            {
                return false;
            }

            if (!long.TryParse(isbn10.Substring(0, isbn10.Length - 1), out var temp))
            {
                return false;
            }

            var sum = 0;
            for (var i = 0; i < 9; i++)
            {
                sum += (isbn10[i] - '0') * (i + 1);
            }

            var result = false;
            var remainder = sum % 11;
            var lastChar = isbn10[isbn10.Length - 1];

            if (lastChar == 'X')
            {
                result = (remainder == 10);
            }
            else if (int.TryParse(lastChar.ToString(), out sum))
            {
                result = (remainder == lastChar - '0');
            }

            return result;
        }

        public static bool IsValidIsbn13(this string isbn13)
        {
            if (string.IsNullOrEmpty(isbn13))
            {
                return false;
            }

            if (isbn13.Contains("-"))
            {
                isbn13 = isbn13.Replace("-", string.Empty);
            }

            if (isbn13.Length != 13)
            {
                return false;
            }

            if (!long.TryParse(isbn13, out var temp))
            {
                return false;
            }

            var sum = 0;
            for (var i = 0; i < 12; i++)
            {
                sum += (isbn13[i] - '0') * (i % 2 == 1 ? 3 : 1);
            }

            var remainder = sum % 10;
            var checkDigit = 10 - remainder;
            if (checkDigit == 10)
            {
                checkDigit = 0;
            }
            var result = (checkDigit == isbn13[12] - '0');
            return result;
        }
    }
}
