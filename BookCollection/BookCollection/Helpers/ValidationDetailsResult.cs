﻿using BookCollection.Constants;
using BookCollection.Models.ApiResponses.Base;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Helpers
{
    public class ValidationDetailsResult : IActionResult
    {
        public async Task ExecuteResultAsync(ActionContext context)
        {
            var modelStateEntries = context.ModelState.Where(e => e.Value.Errors.Count > 0).ToArray();
            
            if (modelStateEntries.Any())
            {
                await context.HttpContext.WriteJsonResponseAsync(400, new APIResult<string>(ProjectErrorCodes.InvalidPayload));
            }
            await Task.CompletedTask;
        }
    }
}
