﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookCollection.Helpers
{
    public static class HttpContextExtentions
    {
        public static async Task WriteJsonResponseAsync<T>(this HttpContext context, int statusCode, T jsonObject)
        {
            context.Response.StatusCode = statusCode;
            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(JsonConvert.SerializeObject(jsonObject), Encoding.UTF8);
        }
    }
}
