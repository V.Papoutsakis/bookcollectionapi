﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Constants
{
    public enum ProjectErrorCodes
    {
        Exception,
        GenericError,
        InvalidPayload,
        NotExisting
    }
}
