﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.CustomAttributes
{
    public class EmptyStringValidationAttribute : ValidationAttribute
    {
        private bool _allowNull { get; set; }
        public EmptyStringValidationAttribute(bool allowNull = false)
        {
            _allowNull = allowNull;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if ((_allowNull && value == null) || (_allowNull && !string.IsNullOrEmpty((string)value) && !((string)value).All(d=>Char.IsWhiteSpace(d))) || (!_allowNull && value != null && !((string)value).All(d => Char.IsWhiteSpace(d))))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
