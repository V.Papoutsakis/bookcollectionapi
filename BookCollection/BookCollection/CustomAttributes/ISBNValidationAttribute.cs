﻿using BookCollection.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.CustomAttributes
{
    public class ISBNValidationAttribute : ValidationAttribute
    {
        private bool _allowNull { get; set; }
        public ISBNValidationAttribute(bool allowNull = false)
        {
            _allowNull = allowNull;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if ((_allowNull && value == null) || (_allowNull && !string.IsNullOrEmpty((string)value) && ((string)value).CheckIsbn()) || (!_allowNull && value!=null && ((string)value).CheckIsbn()))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
