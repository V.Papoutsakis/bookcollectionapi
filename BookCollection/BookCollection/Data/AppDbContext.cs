﻿using BookCollection.Models.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Data
{
    public class AppDbContext : DbContext
    {
        private readonly string _schema;

        public AppDbContext(DbContextOptions<AppDbContext> options, IConfiguration config)
            : base(options)
        {
            _schema = config.GetConnectionString("SchemaPgSQLConnection");
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Author> Authors { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema(_schema);

            builder.Entity<Book>().HasIndex(d => d.ISBN).IsUnique();
            builder.Entity<Author>().HasIndex(d => d.Email).IsUnique();
            builder.Entity<Publisher>().HasIndex(d => d.Email).IsUnique();

            base.OnModelCreating(builder);
        }
    }
}
