﻿using BookCollection.Data;
using BookCollection.Models.ApiRequests;
using BookCollection.Models.Database;
using BookCollection.Models.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Services
{
    public class AuthorsService
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<PublishersService> _logger;

        public AuthorsService(AppDbContext appDbContext, ILogger<PublishersService> logger)
        {
            _appDbContext = appDbContext;
            _logger = logger;
        }

        public async Task<InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>> CreateAndBind(AuthorCreate authorReq) 
        {
            var resultAuthor = await Create(authorReq);

            if (!resultAuthor.Status)
            {
                //Error Case
                return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(resultAuthor.Error);
            }

            bool sendChangesFlag = false;
            List<Guid> bindedBooks = new List<Guid>();
            if (authorReq.BookIdsBind!=null && authorReq.BookIdsBind.Count()>0)
            {
                var books = await _appDbContext.Books.Where(d => authorReq.BookIdsBind.Contains(d.Id)).ToListAsync();
                if (books!=null && books.Count()>0)
                {
                    foreach (var item in books)
                    {
                        item.SetAuthorId(resultAuthor.Data);
                        bindedBooks.Add(item.Id);
                    }
                    sendChangesFlag = true;
                }
            }

            if (authorReq.BookISBNsBind != null && authorReq.BookISBNsBind.Count() > 0)
            {
                var books = await _appDbContext.Books.Where(d => authorReq.BookISBNsBind.Contains(d.ISBN) && !bindedBooks.Contains(d.Id)).ToListAsync();
                if (books != null && books.Count() > 0)
                {
                    foreach (var item in books)
                    {
                        item.SetAuthorId(resultAuthor.Data);
                        bindedBooks.Add(item.Id);
                    }
                    sendChangesFlag = true;
                }
            }

            if (sendChangesFlag)
            {
                var bindingResults = await _appDbContext.SaveChangesAsync();
                if (bindingResults > 0)
                {
                    //Bindings Occured
                    return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(
                        new CreateEntityAndBindingsResult<Guid>(
                                resultAuthor.Data,
                                bindedBooks.Select(d => d).ToList()
                            ));
                } else {
                    //No Bindind Occured
                    return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(
                     new CreateEntityAndBindingsResult<Guid>(
                                 resultAuthor.Data
                             ));
                }
            } else {
                //No Bindind Occured
                return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(
                    new CreateEntityAndBindingsResult<Guid>(
                                resultAuthor.Data
                            ));
            }
        }

        public async Task<InternalDataTransfer<Guid>> Create(AuthorCreate authorReq)
        {
            var author = new Author
            {
                BirthDate = authorReq.BirthDate,
                Email = authorReq.Email ?? null,
                ImageUrl = authorReq.ImageUrl ?? null,
                FirstName = authorReq.FirstName,
                LastName = authorReq.LastName,
                DeathDate = authorReq.DeathDate ?? null,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            return await Create(author);
        }

        public async Task<InternalDataTransfer<Guid>> Create(Author author)
        {
            var error = new InternalDataTransfer<Guid>(false, null);

            _appDbContext.Authors.Add(author);

            try
            {
                var res = await _appDbContext.SaveChangesAsync();
                if (res > 0)
                {
                    return new InternalDataTransfer<Guid>(author.Id);
                }
                else
                {
                    return error;
                }
            }
            catch (Exception e)
            {
                _logger.LogDebug($"Exception {e.Message} {e.StackTrace}");
                return new InternalDataTransfer<Guid>(e);
            }
        }
    }
}
