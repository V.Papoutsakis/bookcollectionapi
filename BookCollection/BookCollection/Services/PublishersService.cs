﻿using BookCollection.Data;
using BookCollection.Models.ApiRequests;
using BookCollection.Models.Database;
using BookCollection.Models.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Services
{
    public class PublishersService
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<PublishersService> _logger;

        public PublishersService(AppDbContext appDbContext, ILogger<PublishersService> logger)
        {
            _appDbContext = appDbContext;
            _logger = logger;
        }

        public async Task<InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>> CreateAndBind(PublisherCreate publisherReq)
        {
            var resultPublisher = await Create(publisherReq);

            if (!resultPublisher.Status)
            {
                //Error Case
                return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(resultPublisher.Error);
            }

            bool sendChangesFlag = false;
            List<Guid> bindedBooks = new List<Guid>();
            if (publisherReq.BookIdsBind != null && publisherReq.BookIdsBind.Count() > 0)
            {
                var books = await _appDbContext.Books.Where(d => publisherReq.BookIdsBind.Contains(d.Id)).ToListAsync();
                if (books != null && books.Count() > 0)
                {
                    foreach (var item in books)
                    {
                        item.SetPublisherId(resultPublisher.Data);
                        bindedBooks.Add(item.Id);
                    }
                    sendChangesFlag = true;
                }
            }

            if (publisherReq.BookISBNsBind != null && publisherReq.BookISBNsBind.Count() > 0)
            {
                var books = await _appDbContext.Books.Where(d => publisherReq.BookISBNsBind.Contains(d.ISBN) && !bindedBooks.Contains(d.Id)).ToListAsync();
                if (books != null && books.Count() > 0)
                {
                    foreach (var item in books)
                    {
                        item.SetPublisherId(resultPublisher.Data);
                        bindedBooks.Add(item.Id);
                    }
                    sendChangesFlag = true;
                }
            }

            if (sendChangesFlag)
            {
                var bindingResults = await _appDbContext.SaveChangesAsync();
                if (bindingResults > 0)
                {
                    //Bindings Occured
                    return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(
                        new CreateEntityAndBindingsResult<Guid>(
                                resultPublisher.Data,
                                bindedBooks
                            ));
                }
                else
                {
                    //No Bindind Occured
                    return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(
                     new CreateEntityAndBindingsResult<Guid>(
                                 resultPublisher.Data
                             ));
                }
            }
            else
            {
                //No Bindind Occured
                return new InternalDataTransfer<CreateEntityAndBindingsResult<Guid>>(
                    new CreateEntityAndBindingsResult<Guid>(
                                resultPublisher.Data
                            ));
            }
        }

        public async Task<InternalDataTransfer<Guid>> Create(PublisherCreate publisherReq)
        {
            var publisher = new Publisher
            {
                Address = publisherReq.Address ?? null,
                Email = publisherReq.Email ?? null,
                ImageUrl = publisherReq.ImageUrl ?? null,
                Name = publisherReq.Name,
                Phone = publisherReq.Phone,
                WebUrl = publisherReq.WebUrl ?? null,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            return await Create(publisher);
        }

        public async Task<InternalDataTransfer<Guid>> Create(Publisher publisher)
        {
            var error = new InternalDataTransfer<Guid>(false, null);
            
            _appDbContext.Publishers.Add(publisher);

            try
            {
                var res = await _appDbContext.SaveChangesAsync();
                if (res > 0)
                {
                    return new InternalDataTransfer<Guid>(publisher.Id);
                }
                else
                {
                    return error;
                }
            }
            catch (Exception e)
            {
                _logger.LogDebug($"Exception {e.Message} {e.StackTrace}");
                return new InternalDataTransfer<Guid>(e);
            }
        }
    }
}
