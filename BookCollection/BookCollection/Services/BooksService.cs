﻿using BookCollection.Constants;
using BookCollection.Data;
using BookCollection.Helpers;
using BookCollection.Models.ApiRequests;
using BookCollection.Models.ApiResponses;
using BookCollection.Models.Database;
using BookCollection.Models.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Services
{
    public class BooksService
    {
        private readonly AppDbContext _appDbContext;
        private readonly ILogger<BooksService> _logger;

        public BooksService(AppDbContext appDbContext, ILogger<BooksService> logger) 
        {
            _appDbContext = appDbContext;
            _logger = logger;
        }

        public async Task<InternalDataTransfer<Guid>> Create(BookCreate bookReq) 
        {
            var book = new Book
            {
                AuthorId = bookReq.AuthorId ?? null,
                Description = bookReq.Description ?? null,
                ImageUrl = bookReq.ImageUrl ?? null,
                ISBN = bookReq.ISBN,
                Title = bookReq.Title,
                PublisherId = bookReq.PublisherId ?? null,
                ReleaseDate = bookReq.ReleaseDate ?? null,
                Visible = bookReq.Visible,
                Status = bookReq.Status,
                Order = bookReq.Order,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
            return await Create(book);
        }

        public async Task<InternalDataTransfer<Guid>> Create(Book book) 
        {
            var error = new InternalDataTransfer<Guid>(false, null);
            if (book.PublisherId.HasValue)
            {
                if (!(await _appDbContext.Publishers.CountAsync(d => d.Id == book.PublisherId.Value) > 0))
                {
                    _logger.LogDebug($"Publisher Not Existing Id: {book.PublisherId.Value}");
                    return error;
                } 
            }

            if (book.AuthorId.HasValue)
            {
                if (!(await _appDbContext.Authors.CountAsync(d => d.Id == book.AuthorId.Value) > 0))
                {
                    _logger.LogDebug($"Author Not Existing Id: {book.AuthorId.Value}");
                    return error;
                }
            }

            _appDbContext.Books.Add(book);

            try
            {
                var res = await _appDbContext.SaveChangesAsync();
                if (res > 0)
                {
                    return new InternalDataTransfer<Guid>(book.Id);
                }
                else
                {
                    return error;
                }
            }
            catch (Exception e)
            {
                return new InternalDataTransfer<Guid>(e);
            }
        }

        public async Task<InternalDataTransfer<Guid>> Update(BookUpdate bookReq)
        {
            var book = await _appDbContext.Books.Where(d => d.Id == bookReq.Id).FirstOrDefaultAsync();
            if (book == null)
            {
                return new InternalDataTransfer<Guid>(false, "NOT_EXISTING");
            }

            if (bookReq.RemoveAuthor)
            {
                book.AuthorId = null;
            }
            if (bookReq.RemovePublisher)
            {
                book.PublisherId = null;
            }

            if (bookReq.ISBN != null && !bookReq.ISBN.CheckIsbn())
            {
                return new InternalDataTransfer<Guid>(false, "INVALID_ISBN");
            }

            book.ImageUrl = bookReq.ImageUrl != null ? bookReq.ImageUrl : book.ImageUrl; 
            book.ISBN = bookReq.ISBN != null ? bookReq.ISBN : book.ISBN; 
            book.PublisherId = bookReq.PublisherId != null ? bookReq.PublisherId : book.PublisherId; 
            book.AuthorId = bookReq.AuthorId != null ? bookReq.AuthorId : book.AuthorId; 
            book.Description = bookReq.Description != null ? bookReq.Description : book.Description; 
            book.Title = bookReq.Title != null ? bookReq.Title : book.Title; 
            book.Visible = bookReq.Visible.HasValue  ? bookReq.Visible.Value : book.Visible; 
            book.Status = bookReq.Status.HasValue ? bookReq.Status.Value : book.Status;
            book.Order = bookReq.Order.HasValue ? bookReq.Order.Value : book.Order;
            book.Updated = DateTime.Now; 
            
            return await Update(book);
        }

        public async Task<InternalDataTransfer<Guid>> Update(Book book)
        {
            var error = new InternalDataTransfer<Guid>(false, null);
            if (book.PublisherId.HasValue)
            {
                if (!(await _appDbContext.Publishers.CountAsync(d => d.Id == book.PublisherId.Value) > 0))
                {
                    _logger.LogDebug($"Publisher Not Existing Id: {book.PublisherId.Value}");
                    return error;
                }
            }

            if (book.AuthorId.HasValue)
            {
                if (!(await _appDbContext.Authors.CountAsync(d => d.Id == book.AuthorId.Value) > 0))
                {
                    _logger.LogDebug($"Author Not Existing Id: {book.AuthorId.Value}");
                    return error;
                }
            }

            _appDbContext.Books.Update(book);

            try
            {
                var res = await _appDbContext.SaveChangesAsync();
                if (res > 0)
                {
                    return new InternalDataTransfer<Guid>(book.Id);
                }
                else
                {
                    return error;
                }
            }
            catch (Exception e)
            {
                return new InternalDataTransfer<Guid>(e);
            }
        }

        public async Task<InternalDataTransfer<Guid>> Delete(Guid bookId)
        {
            var checkBook = await _appDbContext.Books.Where(d => d.Id == bookId).FirstOrDefaultAsync();
            if (checkBook==null)
            {
                return new InternalDataTransfer<Guid>(false, "NOT_EXISTING");
            }

            return await Delete(checkBook);
        }

        public async Task<InternalDataTransfer<Guid>> Delete(Book book) 
        {
            if (book==null)
            {
                return new InternalDataTransfer<Guid>(false, "NOT_NULL");
            }
            var theId = book.Id;

            _appDbContext.Remove(book);

            try
            {
                var check = await _appDbContext.SaveChangesAsync();

                if (check > 0)
                {
                    return new InternalDataTransfer<Guid>(theId);
                } else {
                    return new InternalDataTransfer<Guid>(false, null);
                }
            }
            catch (Exception e)
            {
                return new InternalDataTransfer<Guid>(e);
            }
        }

        public async Task<InternalDataTransfer<List<BookListItemResult>>> GetBookList(OrderChoice choice) 
        {
            List<Book> finalResBooks = new List<Book>();
            var resultsQuery = _appDbContext.Books.Where(d=>d.PublisherId!=null && d.AuthorId!=null && d.Status && d.Visible).Include(d=>d.Author);
            switch (choice)
            {
                case OrderChoice.ALPHA:
                    finalResBooks = await resultsQuery.ToListAsync();
                    finalResBooks = finalResBooks.OrderBy(d => d.Author.LastName).ToList();
                    break;
                case OrderChoice.ALPHACUSTOMORDER:
                    finalResBooks = await resultsQuery.ToListAsync();
                    finalResBooks = finalResBooks.OrderBy(d => d.Author.LastName).ToList();
                    var data  = finalResBooks.GroupBy(d=>d.AuthorId).Select(group =>
                        new {
                            ValKey = group.Key,
                            Data = group.OrderByDescending(x => x.Order).ToList()
                        });
                    var authorsIdsInOrder = finalResBooks.Select(d => d.AuthorId).Distinct();
                    finalResBooks = new List<Book>();
                    foreach (var item in authorsIdsInOrder)
                    {
                        finalResBooks.AddRange(data.Where(d=>d.ValKey == item).SelectMany(d=>d.Data).ToList().OrderByDescending(d=>d.Order));
                    }
                    break;
            }

            return new InternalDataTransfer<List<BookListItemResult>>(BookListItemResult.BuildListResults(finalResBooks));
        }

        public async Task<InternalDataTransfer<BookItemDetailsResult>> GetBookDetails(Guid bookId)
        {
            var book = await _appDbContext.Books.Where(d => d.Id == bookId).Include(d=>d.Author).Include(d => d.Publisher).FirstOrDefaultAsync();
            if (book == null)
            {
                return new InternalDataTransfer<BookItemDetailsResult>(false, "NOT_EXISTING");
            }

            return new InternalDataTransfer<BookItemDetailsResult>(new BookItemDetailsResult(book));
        }
    }
}
