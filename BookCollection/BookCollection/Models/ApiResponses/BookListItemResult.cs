﻿using BookCollection.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Models.ApiResponses
{
    public class BookListItemResult
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ISBN { get; set; }
        public string AuthorFullName { get; set; }

        public BookListItemResult()
        {
        }

        public BookListItemResult(Book book)
        {
            Title = book.Title;
            Description = (book.Description != null && book.Description.Count() > 100) ? $"{book.Description.Substring(0, 100)}..." : book.Description;
            ISBN = book.ISBN;
            AuthorFullName = (book.Author != null) ? $"{book.Author.LastName} {book.Author.FirstName}" : null;
        }

        public static List<BookListItemResult> BuildListResults(List<Book> books) 
        {
            var result = new List<BookListItemResult>();
            foreach (var item in books)
            {
                result.Add(new BookListItemResult(item));
            }
            return result;
        }
    }
}
