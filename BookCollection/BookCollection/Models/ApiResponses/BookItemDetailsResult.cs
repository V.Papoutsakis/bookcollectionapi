﻿using BookCollection.Helpers;
using BookCollection.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Models.ApiResponses
{
    public class BookItemDetailsResult
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ISBN { get; set; }
        public string CreatedView { get; set; }
        public string AuthorsFullName { get; set; }
        public string AuthorEmail { get; set; }
        public string AuthorBirthDate { get; set; }
        public string PublisherName { get; set; }
        public string PublisherAddress { get; set; }

        public BookItemDetailsResult()
        {
        }

        public BookItemDetailsResult(Book book)
        {
            Title = book.Title;
            Description = book.Description;
            CreatedView = book.Created.ToString("dd/MM/yyyy");
            ISBN = book.ISBN;
            AuthorsFullName = book.Author != null ? $"{book.Author.LastName} {book.Author.FirstName}" : "" ;
            AuthorEmail = book.Author != null && book.Author.Email != null ? book.Author.Email : "";
            AuthorBirthDate = book.Author != null && book.Author.BirthDate != null ? book.Author.BirthDate.GetCustomFormatView() : "";
            PublisherName = book.Publisher != null && book.Publisher.Name != null ? book.Publisher.Name : "";
            PublisherAddress = book.Publisher != null && book.Publisher.Address != null ? book.Publisher.Address : "";
        }
    }
}
