﻿using BookCollection.CustomAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Models.Database
{
    public class Book
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [EmptyStringValidation]
        [MaxLength(25)]
        public string Title { get; set; }
        [Required]
        [EmptyStringValidation]
        [MaxLength(200)]
        public string Description { get; set; }
        [Required]
        [ISBNValidation]
        public string ISBN { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public bool Status { get; set; }
        public bool Visible { get; set; }
        public string ImageUrl { get; set; }
        public int Order { get; set; }
        public Guid? AuthorId { get; set; }
        public Guid? PublisherId { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }

        public virtual Author Author { get; set; }
        public virtual Publisher Publisher { get; set; }

        public void SetAuthorId(Guid data)
        {
            AuthorId = data;
        }
        
        public void SetPublisherId(Guid data)
        {
            PublisherId = data;
        }
    }
}
