﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Models.DTO
{
    public class CreateEntityAndBindingsResult<T>
    {
        public CreateEntityAndBindingsResult()
        {
        }

        public CreateEntityAndBindingsResult(Guid id, List<T> data = null)
        {
            Id = id;
            BindingsAdded = data;
        }

        public Guid? Id { get; set; } = null;
        public List<T> BindingsAdded { get; set; } = null;
    }
}
