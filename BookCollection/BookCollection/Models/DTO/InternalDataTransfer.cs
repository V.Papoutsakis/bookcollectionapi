﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Models.DTO
{
    public class InternalDataTransfer<T>
    {
        public bool Status { get; set; }
        public T Data { get; set; }
        public IntrnalErrorObject Error { get; set; }

        public InternalDataTransfer()
        {
        }

        public InternalDataTransfer(T data)
        {
            Status = true;
            Data = data;
        }

        public InternalDataTransfer(Exception e)
        {
            Status = false;
            Error = new IntrnalErrorObject(e);
        }

        public InternalDataTransfer(Exception e, string description)
        {
            Status = false;
            Error = new IntrnalErrorObject(e, description);
        }

        public InternalDataTransfer(bool status, string description)
        {
            Status = status;
            if (!status)
            {
                Error = new IntrnalErrorObject(description);
            }
        }

        public InternalDataTransfer(IntrnalErrorObject intrnalErrorObject)
        {
            Status = false;
            Error = intrnalErrorObject;
        }
    }

    public class IntrnalErrorObject
    {
        public string Error { get; set; }
        public string Description { get; set; }
        public bool IsExceptionTypeError { get; set; }

        public IntrnalErrorObject()
        {
        }

        public IntrnalErrorObject(Exception e)
        {
            Error = e.Message;
            Description = e.StackTrace;
            IsExceptionTypeError = true;
        }

        public IntrnalErrorObject(Exception e, string description)
        {
            if (description == null)
            {
                Error = e.Message;
                Description = e.StackTrace;
                IsExceptionTypeError = true;
            }
            else
            {
                Error = $"Message: { e.Message }, StackTrace: { e.StackTrace }";
                Description = description;
                IsExceptionTypeError = true;
            }

        }

        public IntrnalErrorObject(string description)
        {
            Error = "Error";
            Description = description;
            IsExceptionTypeError = false;
        }
    }
}
