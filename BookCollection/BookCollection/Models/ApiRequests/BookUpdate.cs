﻿using BookCollection.CustomAttributes;
using BookCollection.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookCollection.Models.ApiRequests
{
    public class BookUpdate
    {
        [Required]
        public Guid Id { get; set; }
        [MaxLength(25)]
        [EmptyStringValidation(true)]
        public string Title { get; set; }
        [MaxLength(200)]
        [EmptyStringValidation(true)]
        public string Description { get; set; }
        [ISBNValidation(true)]
        public string ISBN { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public bool? Status { get; set; }
        public bool? Visible { get; set; }
        public string ImageUrl { get; set; }
        public int? Order { get; set; }
        public Guid? AuthorId { get; set; }
        public bool RemoveAuthor { get; set; } = false;
        public Guid? PublisherId { get; set; }
        public bool RemovePublisher { get; set; } = false;
    }
}
