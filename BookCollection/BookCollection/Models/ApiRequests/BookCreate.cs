﻿using BookCollection.CustomAttributes;
using BookCollection.Helpers;
using BookCollection.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BookCollection.Models.ApiRequests
{
    public class BookCreate
    {
        [Required]
        [EmptyStringValidation]
        [MaxLength(25)]
        public string Title { get; set; }
        [Required]
        [EmptyStringValidation]
        [MaxLength(200)]
        public string Description { get; set; }
        [Required]
        [ISBNValidation]
        public string ISBN { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public bool Status { get; set; }
        public bool Visible { get; set; }
        public string ImageUrl { get; set; }
        public int Order { get; set; }
        public Guid? AuthorId { get; set; }
        public Guid? PublisherId { get; set; }
    }
}
