﻿using BookCollection.Helpers;
using BookCollection.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BookCollection.Models.ApiRequests
{
    public class PublisherCreate
    {
        [Required]
        [MaxLength(25)]
        public string Name { get; set; }
        [Required]
        [MaxLength(25)]
        public string Address { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ImageUrl { get; set; }
        public string WebUrl { get; set; }
        public List<Guid> BookIdsBind { get; set; } = null;
        public List<string> BookISBNsBind { get; set; } = null;
    }
}
