﻿using BookCollection.Helpers;
using BookCollection.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BookCollection.Models.ApiRequests
{
    public class AuthorCreate
    {
        [Required]
        [MaxLength(15)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(25)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public List<Guid> BookIdsBind { get; set; } = null;
        public List<string> BookISBNsBind { get; set; } = null;
        [Required]
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }
        public string ImageUrl { get; set; }
    }
}
