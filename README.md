# BookCollectionApi Using Docker


> Docker Build And Run

`docker volume create --name=postgres-bookscollection`

`cd to\local.yml\file`

`docker-compose -f .\local.yml up --build`

>| Notes | 

`Host Machine`
- Database Host: localhost 
- Database Port: 8002
- Database User: postgres
- Database Password: password

> Swagger UI 

`http://localhost:5005/swagger`



# BookCollectionApi Without Docker With Other Postgres Server


> Database User Preparation

`Create User`

`Create Database`

`Update Connection String To appsettings.Development.json`

`Publish Application In Debug Mode (Development Enviroment)`

`After Successfull App Start => http://localhost:5000/api/dev/up_migrate`


> Swagger UI 

`http://localhost:5000/swagger`
